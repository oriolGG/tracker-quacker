/**
 * Modifies a blocklist to add non-duplicate domains from the provided list.
 *
 * @param {string[]} domains List of domains to add.
 * @param {Object} blockList Key (first character of a domain) / value (list of domains) pairs to classify domains to block.
 */
module.exports = function (domains, blockList) {
  for (let domain of domains) {
    // Group the domains by their initial character for easier access.
    const initial = domain[0];
    if (!blockList[initial]) {
      blockList[initial] = [];
    }
    if (blockList[initial].includes(domain)) {
      // Avoid duplicates
      continue;
    }
    blockList[initial].push(domain);
  }
};
