// Builds the blockList from DuckDuckGo's Tracker Radar repository
const fs = require("fs");
const { execSync } = require("child_process");
const radarParser = require("./radarParser.js");
const addToBlockList = require("./addToBlockList.js");

const radarDir = "tracker-radar";

if (!fs.existsSync(radarDir)) {
  console.log("Tracker Radar data not present, cloning the repository.");
  execSync("git clone https://github.com/duckduckgo/tracker-radar.git", {
    stdio: [0, 1, 2], // Print the command output.
  });
}

const blockList = {};

const regions = fs.readdirSync(`${radarDir}/domains/`);

for (let region of regions) {
  console.log(`Parsing domains for region ${region}`);
  const regionDir = `${radarDir}/domains/${region}`;

  const domainFiles = fs.readdirSync(regionDir);

  for (let file of domainFiles) {
    let content = JSON.parse(fs.readFileSync(`${regionDir}/${file}`, "utf-8"));
    const domainsToBlock = radarParser(content);
    addToBlockList(domainsToBlock, blockList);
  }
}

fs.writeFileSync("public/data/blockList.json", JSON.stringify(blockList));

const numOfDomainsBlocked = Object.values(blockList).flat().length;
console.log(
  `Added a total of ${numOfDomainsBlocked} domains to the blockList.`
);
console.log("BlockList saved in /public/data/blockList.json");
