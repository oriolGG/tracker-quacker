/**
 * Obtains the domains, including subdomains, from the contents of a DuckDuckGo Tracker Radar domain json file.
 * For the first version of this project, we will not block domains that have the CDN category.

 * @param {Object} content JSON from the file.
 * @return {string[]} List of domains to block.
 */
module.exports = function (content) {
  const rootDomain = content.domain;
  const subdomains = content.subdomains;
  const categories = content.categories;
  if (categories.includes("CDN")) {
    return [];
  }
  const domainsToBlock = [
    rootDomain,
    ...subdomains.map((sd) => `${sd}.${rootDomain}`),
  ];
  return domainsToBlock;
};
