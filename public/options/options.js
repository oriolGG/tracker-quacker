import allowList from "../scripts/allowList.js";

const listSection = document.querySelector("#allowList");

// Obtain the initial state for the current page.
allowList.get((list) => {
  renderAllowedDomainsList(list);
});

function renderAllowedDomainsList(allowedDomains) {
  if (allowedDomains.length === 0) {
    listSection.innerHTML =
      "<p>None, currently Tracker Quacker is blocking trackers in all websites.</p>";
    return;
  }
  listSection.innerHTML = allowedDomains.map(allowedDomainRow).join("");

  const removeDomainBtns = document.querySelectorAll(".remove-btn");
  Array.from(removeDomainBtns).forEach((btn) =>
    btn.addEventListener("click", () => {
      const domain = btn.getAttribute("data-domain");
      allowList.removeDomain(domain);
      // We need to rerender the list to reflect the updated allowList.
      renderAllowedDomainsList(allowedDomains.filter((d) => d !== domain));
    })
  );
}

function allowedDomainRow(domain) {
  return `
  <div class="domain-row">
    <p>${domain}</p>
    <a class="remove-btn" data-domain="${domain}">Block</a>
  </div>`;
}
