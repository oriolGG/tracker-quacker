import allowList from "../scripts/allowList.js";
import {
  browser,
  getCurrentTabUrl,
  setExtensionIcon,
} from "../scripts/browser.js";
import { getDomainFromUrl } from "../scripts/domains.js";

let areTrackersAllowed = false;
let currentDomain; // 'www.example.com'

// Obtain the initial state for the current page.
allowList.get((list) =>
  getCurrentTabUrl((url) => {
    currentDomain = getDomainFromUrl(url);
    areTrackersAllowed = list.includes(currentDomain);
    displayTrackersState(areTrackersAllowed);
    // Preload class blocks the slider animations during initial load.
    setTimeout(() => {
      const slider = document.querySelector(".slider");
      slider.classList.remove("preload");
    }, 500);
  })
);

const toggleTrackersCheckbox = document.querySelector(
  "#toggleTrackersCheckbox"
);
toggleTrackersCheckbox.addEventListener("change", function (e) {
  areTrackersAllowed = !e.target.checked;
  if (areTrackersAllowed) {
    allowList.addDomain(currentDomain);
  } else {
    allowList.removeDomain(currentDomain);
  }
  displayTrackersState(areTrackersAllowed);
});

// Allow the user to navigate to the extension options.
document.querySelector("#optionsButton").addEventListener("click", () => {
  browser.runtime.openOptionsPage();
});

// Handle the "Report broken page" section.
// Currently, it just logs the action in the console and shows the user a thank you message.
// TODO: before release, this should send analytics to our data infrastructure.
document
  .querySelector("#reportBrokenPageButton")
  .addEventListener("click", reportBrokenPage);

function reportBrokenPage() {
  console.log(`Website ${currentDomain} reported as broken.`);
  document.querySelector("#reportBrokenPage").innerHTML =
    "<p>Reported. Thank you!</p>";
}

/**
 * Rerenders all the UI elements that have state depending on trackers being allowed for this site, mainly:
 * - The extension logo image and the extension icon
 * - The sentence that says if trackers are allowed or blocked
 * - The toggle
 *
 * @param {boolean} areTrackersAllowed
 */
function displayTrackersState(areTrackersAllowed) {
  const pageStateDisplay = document.querySelector("#pageState");
  const logo = document.querySelector(".logo");
  pageStateDisplay.innerHTML = areTrackersAllowed ? "allowed" : "blocked";
  toggleTrackersCheckbox.checked = !areTrackersAllowed;
  if (areTrackersAllowed) {
    pageStateDisplay.classList.add("text-warning");
    pageStateDisplay.classList.remove("text-success");
    logo.src = "/images/duck-warning-small.png";
    setExtensionIcon("duck-warning.png");
  } else {
    pageStateDisplay.classList.add("text-success");
    pageStateDisplay.classList.remove("text-warning");
    logo.src = "/images/duck-success-small.png";
    setExtensionIcon("duck-success.png");
  }
}
