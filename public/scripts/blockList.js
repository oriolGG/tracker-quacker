import { getDomainFromUrl, getRootFromDomain } from "./domains.js";

/**
 * Returns true if a request should be blocked.
 * Allows requests in the following cases:
 * - The request is initiated from a Chrome extension.
 * - The user has allowed trackers for the domain of the initiator.
 * - The target belongs to the same root domain as the initiator, even if the target is in the blockList.
 * - Finally, even if it's a third party targetUrl, we allow the request if it is not found in the blockList.
 *
 * @param {string} initiatorUrl The source of the request, aka the current url
 * @param {string} targetUrl The target of the request
 * @param {Object} blockList Key (first character of a domain) / value (list of domains) pairs to classify domains to block.
 * @param {string[]} allowedDomains List of sites where the user has allowed trackers.
 * @returns {boolean}
 */
export function shouldBlockRequest(
  initiatorUrl,
  targetUrl,
  blockList,
  allowedDomains
) {
  // We return early if the trigger comes from an extension to simply the following flow.
  if (initiatorUrl.startsWith("chrome-extension://")) {
    return false;
  }

  const currentDomain = getDomainFromUrl(initiatorUrl);

  // If the user has allowed trackers for the current domain, we skip further checks.
  if (allowedDomains.includes(currentDomain)) {
    return false;
  }

  // If the target has the current root domain, we don't block it
  // to minimize breaking sites that belong to domains in our blockList
  const targetDomain = getDomainFromUrl(targetUrl);
  if (getRootFromDomain(targetDomain) == getRootFromDomain(currentDomain)) {
    return false;
  }

  // Everything that reaches this point is candidate to be a third party tracker.
  return isInTheBlockList(targetDomain, blockList);
}

/**
 * Returns true if the domain can be found in the blockList.
 *
 * @param {string} domain Domain to check.
 * @param {Object} blockList Key (first character of a domain) / value (list of domains) pairs to classify domains to block.
 * @returns {boolean}
 */
function isInTheBlockList(domain, blockList) {
  const initialChar = domain[0];
  return !!blockList[initialChar] && blockList[initialChar].includes(domain);
}
