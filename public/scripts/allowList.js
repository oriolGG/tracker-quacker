import { browser } from "./browser.js";

/**
 * Object to interact with the list of sites where the user has allowed trackers, stored in the browser local storage.
 */
export default {
  get(callback) {
    browser.storage.local.get("allowList", function (data) {
      const allowList = data.allowList || [];
      callback(allowList);
    });
  },
  save(list) {
    browser.storage.local.set({ allowList: list });
  },
  listenToUpdates(callback) {
    browser.storage.onChanged.addListener((data) => {
      callback(data.allowList.newValue);
    });
  },
  addDomain(domain) {
    this.get((list) => {
      list.push(domain);
      this.save(list);
    });
  },
  removeDomain(domain) {
    this.get((list) => {
      const listWithoutDomain = list.filter((d) => d !== domain);
      this.save(listWithoutDomain);
    });
  },
};
