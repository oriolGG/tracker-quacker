/**
 * Object with the interface of the browser extension API
 * Currently, only Google Chrome is supported.
 */
// eslint-disable-next-line no-undef
export const browser = chrome;

/**
 * Sets the extension icon on the active tab.
 *
 * @param {string} icon image name of the icon, such as "duck.png".
 */
export function setExtensionIcon(icon) {
  browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    browser.browserAction.setIcon({
      path: `/images/${icon}`,
      tabId: tabs[0].id,
    });
  });
}

/**
 * Obtains the active tab url from the browser API.
 *
 * @param {function(string)} callback function to execute with the obtained url.
 */
export function getCurrentTabUrl(callback) {
  browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    callback(tabs[0].url);
  });
}
