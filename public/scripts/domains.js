import doubleTlds from "../data/tlds.js";

/**
 * Returns the domain of a URL.
 *
 * @param {string} url A url such as https://api.example.com
 * @returns {string} The domain, such as api.example.com
 */
export function getDomainFromUrl(url) {
  const splitted = url.split("/"); // https://www.duckduckgo.com/ => ['https:', '', 'www.duckduckgo.com' ,'']
  return splitted[2]; // 'www.duckduckgo.com'
}

/**
 * Returns the root of a domain.
 *
 * @param {string} domain A domain, such as api.example.com or www.example.uk.com
 * @returns {string} The root of the domain, such as example.com or example.uk.com
 */
export function getRootFromDomain(domain) {
  const splitted = domain.split("."); // www.duckduckgo.com => ['www', 'duckduckgo', 'com']
  if (splitted.length <= 2) {
    return domain;
  }
  const potentialRoot = splitted.slice(splitted.length - 2).join(".");
  // An edge case is that some TLDs include one intermediate dot, such as '.uk.com'.
  if (doubleTlds.includes("." + potentialRoot)) {
    const longTld = "." + potentialRoot;
    return splitted[splitted.length - 3] + longTld;
  }
  return potentialRoot;
}
