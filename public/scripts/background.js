import allowList from "./allowList.js";
import { shouldBlockRequest } from "./blockList.js";
import { browser, setExtensionIcon } from "./browser.js";
import { getDomainFromUrl } from "./domains.js";

let allowedDomains = [];
let blockList = {};

// On install/updates, we want to refresh the allowList and the blockList.
browser.runtime.onInstalled.addListener(async () => {
  allowList.get((list) => (allowedDomains = list));
  blockList = await fetch("../data/blockList.json").then((res) => res.json());
});

// Keep a copy of the allowList on local storage in memory for easier access.
allowList.listenToUpdates((newList) => (allowedDomains = newList));

// Set up the request blocking algorithm
browser.webRequest.onBeforeRequest.addListener(
  handleUrl,
  {
    urls: ["<all_urls>"],
  },
  ["blocking"]
);

// Add the correct icon to new tabs based on the allowList state.
browser.tabs.onUpdated.addListener((_tabId, _changeInfo, tab) => {
  const tabDomain = getDomainFromUrl(tab.url);
  setExtensionIcon(
    allowedDomains.includes(tabDomain) ? "duck-warning.png" : "duck-success.png"
  );
});

console.log("Tracker Quacker initialized.");

/**
 * Decides if a request should be blocked based on its information.
 *
 * @param {RequestDetails} requestDetails information about the request provided by the webRequest.onBeforeRequest listener.
 * @returns {Object} indicates if the request should be cancelled (blocked)
 */
function handleUrl(requestDetails) {
  // The first request to a site contains the string 'null' instead of the initiator.
  const source =
    !requestDetails.initiator || requestDetails.initiator === "null"
      ? requestDetails.url
      : requestDetails.initiator;
  const target = requestDetails.url;
  const cancelRequest = shouldBlockRequest(
    source,
    target,
    blockList,
    allowedDomains
  );
  if (cancelRequest) {
    console.log(`Blocking request to domain ${target}`);
  }
  return { cancel: cancelRequest };
}
