import addToBlockList from "../radar-parser/addToBlockList.js";
import radarParser from "../radar-parser/radarParser.js";

describe("radarParser", () => {
  it("should return a list of the root domain plus all the subdomains for a given file", () => {
    const mockTrackerFile = {
      domain: "0ps.us",
      subdomains: ["opl", "op1", "ts1", "ts2"],
      categories: [],
    };
    const expected = [
      "0ps.us",
      "opl.0ps.us",
      "op1.0ps.us",
      "ts1.0ps.us",
      "ts2.0ps.us",
    ];
    expect(radarParser(mockTrackerFile)).toEqual(expected);
  });
  it("should return an empty list if the domain has the CDN category", () => {
    const mockTrackerFile = {
      domain: "0ps.us",
      subdomains: ["opl", "op1", "ts1", "ts2"],
      categories: ["CDN"],
    };
    const expected = [];
    expect(radarParser(mockTrackerFile)).toEqual(expected);
  });
});

describe("addToBlockList", () => {
  it("should classify domains by initial character", () => {
    const blockList = {};
    let expected = {
      a: ["a123.com"],
    };
    addToBlockList(["a123.com"], blockList);
    expect(blockList).toEqual(expected);

    addToBlockList(["another.com", "facebook.com"], blockList);
    expected = {
      a: ["a123.com", "another.com"],
      f: ["facebook.com"],
    };
    expect(blockList).toEqual(expected);
  });
  it("should avoid duplicates", () => {
    const blockList = { a: ["a123.com"] };
    let expected = {
      a: ["a123.com"],
    };
    addToBlockList(["a123.com", "a123.com"], blockList);
    expect(blockList).toEqual(expected);
  });
});
