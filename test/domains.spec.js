import {
  getDomainFromUrl,
  getRootFromDomain,
} from "../public/scripts/domains.js";

describe("getDomainFromUrl", () => {
  it("should return a domain in format duckduckgo.com", () => {
    expect(getDomainFromUrl("http://duckduckgo.com")).toBe("duckduckgo.com");
    expect(getDomainFromUrl("https://duckduckgo.com/search")).toBe(
      "duckduckgo.com"
    );
  });
  it("should include subdomains", () => {
    expect(getDomainFromUrl("https://test.duckduckgo.com/search")).toBe(
      "test.duckduckgo.com"
    );
  });
});

describe("getRootFromDomain", () => {
  it("should return a root domain in format duckduckgo.com", () => {
    expect(getRootFromDomain("duckduckgo.com")).toBe("duckduckgo.com");
    expect(getRootFromDomain("test.duckduckgo.com")).toBe("duckduckgo.com");
  });
  it("should work with TLDs with an inner dot such as .uk.com", () => {
    expect(getRootFromDomain("duckduckgo.uk.com")).toBe("duckduckgo.uk.com");
    expect(getRootFromDomain("test.duckduckgo.uk.com")).toBe(
      "duckduckgo.uk.com"
    );
  });
});
