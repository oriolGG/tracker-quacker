import { shouldBlockRequest } from "../public/scripts/blockList.js";

const mockBlockList = {
  f: ["facebook.com"],
  1: ["123tracks.com"],
  r: ["reddit.com"],
};
const mockAllowedDomains = ["reddit.com"];

describe("shouldBlockRequest", () => {
  it("should allow requests triggered by a chrome extension", () => {
    const initiatorUrl = "chrome-extension://example-extension";
    const targetUrl = "https://facebook.com";
    expect(
      shouldBlockRequest(
        initiatorUrl,
        targetUrl,
        mockBlockList,
        mockAllowedDomains
      )
    ).toBe(false);
  });
  it("should allow requests triggered by a site in the allowList", () => {
    const initiatorUrl = "https://reddit.com";
    const targetUrl = "https://api.reddit.com";
    expect(
      shouldBlockRequest(
        initiatorUrl,
        targetUrl,
        mockBlockList,
        mockAllowedDomains
      )
    ).toBe(false);
  });
  it("should allow requests to a target of the same root domain as the initiator", () => {
    const initiatorUrl = "https://facebook.com";
    const targetUrl = "https://api.facebook.com";
    expect(
      shouldBlockRequest(
        initiatorUrl,
        targetUrl,
        mockBlockList,
        mockAllowedDomains
      )
    ).toBe(false);
  });
  it("should block requests to third parties belonging to the blockList", () => {
    const initiatorUrl = "https://nytimes.com";
    const targetUrl = "https://123tracks.com/track?id=12345";
    expect(
      shouldBlockRequest(
        initiatorUrl,
        targetUrl,
        mockBlockList,
        mockAllowedDomains
      )
    ).toBe(true);
  });
  it("should allow requests to third parties not belonging to the blockList", () => {
    const initiatorUrl = "https://nytimes.com";
    const targetUrl = "https://notsuspicioustracking.com/track?id=12345";
    expect(
      shouldBlockRequest(
        initiatorUrl,
        targetUrl,
        mockBlockList,
        mockAllowedDomains
      )
    ).toBe(false);
  });
});
