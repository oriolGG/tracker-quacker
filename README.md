# Tracker Quacker

This project is a proof of concept for an online tracker blocker browser extension. For now, only Chrome is supported.

It has the following features:

- Block requests to online trackers (domains defined in DuckDuckGo's [Tracker Radar](https://github.com/duckduckgo/tracker-radar)).
- Allow the user to turn off tracker blocking for a given website from a toolbar popup.
- Allow the user to see which websites have tracker blocking disabled.
  - Shows the list of all websites in the Options page, allowing the user to block them again.
  - Shows a different extension icon in the toolbar for a quick visual cue of blocking state of the current tab.
  - Shows clear visual cues in the popup UI to understand blocking state for a particular website.
- Allow the user to report that a site is broken (currently not connected to any backend).

![popup](docs/tracker-quacker.png)

## Setup

1. Clone this repository
2. Run `npm ci`, this will install the project dependencies and trigger the postinstall script that downloads DuckDuckGo's Tracker Radar data and builds the blockList based on it.
3. The `public` directory is ready for deployment. If you are testing locally, go to `chrome://extensions` > Load unpacked > Select the `public` directory.

NOTE: if you are testing locally, make sure to give permissions of "On all sites" for "Allow this extension to read and change all your data on websites you visit" in the extension settings.

## Project structure

The project is divided in three main directories:

- `public`, containing all the code of the extension.
- `radar-parser`, with the scripts to parse DuckDuckGo's Tracker Radar repository to generate the blockList.
- `test` for all the automated tests.

### Extension anatomy

The extension has a background script and two UI elements: a popup and an options page.
Entry points for those are defined in the extension's `manifest.json`.

The two important pieces of frontend logic that support current behavior are managing the allowed sites list (allowList) in the browser local storage, and blocking the requests that match the blocklisting algorithm.

### Parsing Tracker Radar data

For this proof of concept, the algorithm to parse the Tracker Radar returns all domains (including subdomains) that do not have a category of `CDN`.

The [tracker-radar](https://github.com/duckduckgo/tracker-radar) repository is cloned automatically during project setup. A different approach, probably more scalable, would be to add it as a git submodule.

## Manual testing

This first version has been tested manually in Chrome, using sites that are in and out of the blockList.

Inspecting a site while the extension is blocking trackers, one can see errors of `net::ERR_BLOCKED_BY_CLIENT` indicating that requests are blocked.

Example of non-broken site with trackers blocked, [Nativshark](https://www.nativshark.com/):

![nativshark](docs/nativshark.png)

Sites that are in the blocklist, such as Reddit and the New York Times, while accessible (since the user consciously went there), often show broken since they use different domains for their static assets that are in the blocklist too (without being marked as CDN). This is a limitation of the current proof of concept algorithm, which needs to be improved as future work. This is why the "report broken site" feature is important.

Example of broken site with trackers blocked, [Reddit](https://www.reddit.com/):

![reddit](docs/reddit.png)

## Automated testing

Run the automated tests suite with `npm run test`.

Tests can be found under the `test` directory. The project currently only has unit tests for the core logic, such as the tracker data parser and the blocking algorithm.

I didn't have time to go into integration testing to automate checking the interaction with the browser APIs such as storage and webRequest. For such tests, a first idea is to use headless Chrome tool such as Puppeteer, but maybe there are better approaches.

## CI/CD

The project currently doesn't have any CI/CD setup , however, it is prepared for that:

- Automated project setup with `npm ci`, the only requirement is Node.
- Run automated tests with `npm run test`
- `public` is the directory to deploy.

## Deviations from the technical design document

- In "How to define the blocking criteria", one item in the algorithm to block requests was `In the Tracker Radar, it exists in the domain field of any file`. The implementation has actually been more specific `In the Tracker Radar, it exists in the domain or subdomains + domain files of any file`.
- In "How to manage the allowList based on user actions", an item was `Have the color of the extension icon in the toolbar to change based on being turned on or off for the current page.`. The implementation ended up being changing the icon itself, showing a checkmark if trackers are blocked or showing a cross if allowed, for an easier visual cue.

## Future work

Few ideas for future work are:

- Connect the "Report broken site" feature to a backend, use the data to prioritize blocking algorithm improvements.
- Integration tests with Puppeteer or similar tool.
- Known bug: the extension icon in the toolbar updates correctly when opening a new tab or toggling trackers from the popup. However, when having several tabs from the same domain already open, toggling trackers on one will only update the icon of that active tab. Same happens if the toggling is done from the extension options page. To solve this, we'd need to update the icon for all tabs with the same domain when toggling trackers in one of them, from anywhere.
- Potential bug: in some cases, while reloading the extension often during development, there were small performance issues with the popup initial rendering. Need to look at this more in detail, usual suspects are listeners being added multiple times, causing some costly operation being executed more often than it should.
- Allow users to opt-in for metrics, to understand extension usage from a quantitative point of view.
- Add a feedback form to gather qualitative user feedback.
- Show in the popup how many trackers have been blocked for a given website.
- Firefox support: determine how much extra work would it take to support also Firefox.
